

//1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
// Екранування потрібно для надання декількох лапок у рядку без того, щоб JavaScript неправильно інтерпретував те, і сприймав екрановані лапки як символ для виведення, а не як кінець поточного рядка. Якщо простіше, то екранування потрібно для того, коли всередині рядка нам необхідно поставити, наприклад, кавички, в нас не означало, що це кінець строки. Для екранування якогось символу, потрібно поставити перед цим самим символом зворотну косу межу (\). Це показує інтерпретатор JavaScript, що наступна лапка не є кінцем рядка, а повинна знаходитися всередині рядка.
// Наприклад: const sampleStr = "Марго сказала, \"Діана вивчає JavaScript\".";
// Ми б на консолі отримали: Марго сказала, "Діана вивчає JavaScript".


//2. Які засоби оголошення функцій ви знаєте?
// Function declaration - функція оголошується за допомогою ключового слова function
// Named Function expression - функція записується в змінну, а також має своє ім'я (правда функцію не можна буде викликати по цьому імені).
// Function expression - функція також оголошується за допомогою ключового слова function, але вона не має імені, і вона записується в змінну.


//3. Що таке hoisting, як він працює для змінних та функцій?
// Hoisting, тобто спливання, підняття, це механізм, при якому змінні та оголошення функції піднімаються вгору по своїй області видимості перед виконанням коду. Однією з переваг підйому є те, що він дозволяє нам використовувати функції перед їх оголошенням у коді.


function createNewUser() {
  let userName = prompt("Enter your name");
  let userLastName = prompt("Enter your last name");
  let userDateBirth = new Date(prompt("Enter your date of birth, 'dd.mm.yyyy'"));

  const newUser = {
    firstName: userName,
    lastName: userLastName,
    birthday: userDateBirth.getFullYear(),
    getLogin() {
      return (this.firstName[0] + this.lastName).toLowerCase();
    },
    getAge() {
      let date = new Date();
      return (Number(date.getFullYear()) - Number(this.birthday));
    },
    getPassword() {
      return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() +
        this.birthday);
    },
  }
  return newUser
}

const user = createNewUser()

console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());